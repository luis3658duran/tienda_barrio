import 'package:flutter/material.dart';
import 'package:tienda_barrio/app/modules/login/login_page.dart';
import 'package:get/get.dart';
import 'package:tienda_barrio/app/routers/app_pages.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(),
      getPages: AppPage.pages,
    );
  }
}
