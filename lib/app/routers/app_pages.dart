import 'package:get/get.dart';
import 'package:tienda_barrio/app/modules/login/login_page.dart';

import 'app_routes.dart';

class AppPage {
  static final List<GetPage> pages = [
    GetPage(
      name: AppRoutes.LOGIN,
      page: () => LoginPage(),
      // binding: LoginBinding(),
    ),
  ];
}
